#! /usr/bin/python3

import sys
sys.path.append("/home/goatchurch/caving/survexprocessing")
from parse3ddmp import DMP3d
from basicgeo import P3

import os, time, re, sys, datetime, math, json
import pyproj

f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/Skydusky.3d"
fjs = "/home/goatchurch/geom3d/goatchurchprime.github.io/cardboardlike/Skydusky.js"
f3d = "/home/goatchurch/caving/expoloser/all.3d"
f3d = "/home/goatchurch/caving/NorthernEngland/ThreeCountiesArea/survexdata/all.3d"
f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/40.3d"
f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/tianxingdongxuexitong.3d"
#f3d = "/home/goatchurch/caving/NorthernEngland/ThreeCountiesArea/survexdata/all.3d"
f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/quankou-system.3d"
f3d = "/home/goatchurch/caving/survexprocessing/maxpleasure2.3d"
sendactivity("clearallcontours")
sendactivity("clearallpoints")
sendactivity("clearalltriangles")

dd3d = os.popen("dump3d -d %s" % f3d)
lines = dd3d.readlines()
dmp3d = DMP3d(lines)

p0, dfac = dmp3d.pavg, 0.1
def sh(p):  
    return P3((p[0]-p0[0])*dfac, (p[1]-p0[1])*dfac, p[2]*dfac)

sendactivity(points=[sh(p)  for p, v in dmp3d.nodepmap.items()  if "ENTRANCE" in v[1] ])
sendactivity(contours=[[sh(line[0]), sh(line[1])]  for line in dmp3d.lines  if "SURFACE" not in line[4]])
sendactivity(contours=[[sh(line[0]), sh(line[1])]  for line in dmp3d.lines  if "SURFACE" in line[4]], materialnumber=2)



tris = [ ]
quads = [ ]
ined = 0
def AddQuad(q0, q1, q2, q3):
    tris.append((q0.x,q0.y,q0.z, q1.x,q1.y,q1.z, q2.x,q2.y,q2.z))
    tris.append((q0.x,q0.y,q0.z, q2.x,q2.y,q2.z, q3.x,q3.y,q3.z))
    
for xsect in dmp3d.xsects:
    if len(xsect) < 2:
        continue
    #xvol = sum(vquad(q0, q1, q2, q3)  for q0, q1, q2, q3 in dmp3d.GetXSectionQuadHedron(xsect))
    #if (xvol > 0):  continue
    quads.extend(dmp3d.GetXSectionQuadHedron(xsect))
for q0, q1, q2, q3 in quads:
    AddQuad(sh(q0), sh(q1), sh(q2), sh(q3))

sendactivity(codetriangles=tris)

