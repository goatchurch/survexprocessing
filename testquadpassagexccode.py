#! /usr/bin/python3

import sys
sys.path.append("/home/goatchurch/caving/survexprocessing")
from parse3ddmp import DMP3d
from basicgeo import P3

import os, time, re, sys, datetime, math, json
import pyproj

f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/ewd-system.3d"
#f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/tianxingdongxuexitong.3d"
#f3d = "/home/goatchurch/caving/eurospeleotalk/logbookprocessing/quankou-system.3d"

dd3d = os.popen("dump3d -d %s" % f3d)
lines = dd3d.readlines()
dmp3d = DMP3d(lines)


p0 = max((node[0]  for node in dmp3d.nodes  if "ENTRANCE" in node[2]), default=None, key=lambda X: (X[2], X[0], X[1]))
dfac = 0.1
def sh(p):  
    return P3((p[0]-p0[0])*dfac, (p[1]-p0[1])*dfac, p[2]*dfac)

sendactivity("clearalltriangles")


def quadsequalxcs(quads, xcs):
    ccquads = [ ]
    ccquads.append(tuple(xcs[0]))
    for U, v in zip(xcs, xcs[1:]):
        ccquads.append((v[0], v[1], U[1], U[0]))
        ccquads.append((v[1], v[2], U[2], U[1]))
        ccquads.append((v[2], v[3], U[3], U[2]))
        ccquads.append((v[3], v[0], U[0], U[3]))
    ccquads.append(tuple(reversed(xcs[-1])))

    def mincyc(k):
        i = min(range(4), key=lambda X:k[X])
        return tuple(k[(i+j)%4]  for j in range(4))

    sccquads = sorted(tuple(mincyc(k)  for k in ccquads))
    squads = sorted(tuple(mincyc(k)  for k in quads))
    def eqq(k0, k1):
        for i in range(4):
            if k0 == tuple(k1[(i+j)%4]  for j in range(4)):
                return 1
        return 0

    seqq = sum(eqq(k0, k1)  for k0, k1 in zip(squads, sccquads))
    return seqq == len(quads)


tris = [ ]
quads = [ ]
ined = 0
def AddQuad(q0, q1, q2, q3):
    tris.append((q0.x,q0.y,q0.z, q1.x,q1.y,q1.z, q2.x,q2.y,q2.z))
    tris.append((q0.x,q0.y,q0.z, q2.x,q2.y,q2.z, q3.x,q3.y,q3.z))
    
for i, xsect in enumerate(dmp3d.xsects):
    if len(xsect) < 2:
        continue
    lquads, lxcs = dmp3d.GetXSectionQuadHedronP(xsect)
    quads.extend(lquads)
    if not quadsequalxcs(lquads, lxcs):
        print("bad xsectagreement", i)

for q0, q1, q2, q3 in quads:
    AddQuad(sh(q0), sh(q1), sh(q2), sh(q3))


sendactivity(codetriangles=tris)







    
